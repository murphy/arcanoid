# flow3r imports
from st3m import InputState
from st3m.application import Application, ApplicationContext
from ctx import Context
from st3m.utils import tau
from collections import deque
import st3m.run
import leds
import math
import random
import captouch
import sys_buttons
import struct

class Arkanoid(Application):

    BALL_SIZE = 5


    #Todo: Had issues to get ViewManager to work... --> Working with states instead.
    #0 = Game
    #1 = Score Screen
    #2 = Menu
    CURRENT_STATE = 2

    #0 = IMU 
    #1 = Touch
    CONTROL_TYPE = 0

    DEMO_MODE = False

    SCORE_TEXT = ""
    BRICK_WIDTH = 20
    BRICK_HEIGHT = 10

    CURRENT_LVL = 1

    LEVEL_FILE = "/flash/sys/apps/murphy-arcanoid/levels.dat"

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.player = Player(4)
        self.bricks = self.load_level(self.CURRENT_LVL)
        self.demo_button_pressed = False
        self.control_button_pressed = False
        self.start_button_pressed = False
        self.ball_sticking = True
        self.next_level_pressed = True

        print(f"bundlePath:{app_ctx.bundle_path}")

        

    score_timer = 0

    def draw(self, ctx: Context) -> None:

        if self.CURRENT_STATE == 0:
            
            # Drawing the background
            ctx.rectangle(-120, -120, 240, 240)
            ctx.rgb(1, 1, 1)
            ctx.fill()
            
            if not self.DEMO_MODE:
                ctx.rectangle(-120, 0, 240, 120)
                ctx.rgb(0, 0, 0)
                ctx.fill()


            ctx.arc(0, 0, 117, 0, tau, 0)
            ctx.rgb(1, 1, 1)
            ctx.fill()

            # Drawing the bat for the player
            self._draw_bat(ctx, self.player)

            # Drawing the ball
            ctx.arc(self.p_x, self.p_y, self.BALL_SIZE, 0, tau , 0)
            ctx.rgb(117 / 255, 255 / 255, 226 / 255)
            ctx.fill()

            for brick in self.bricks:
                brick.draw(ctx)
        if self.CURRENT_STATE == 1:
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE
            ctx.font_size = 60
            ctx.font = ctx.get_font_name(5)

            ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

            ctx.rgb(1,1,1)
            ctx.move_to(0, 0)
            ctx.save()
            ctx.text(self.SCORE_TEXT)
            ctx.restore()
            self.score_timer = self.score_timer + 1



        if self.CURRENT_STATE == 2:
            ctx.text_align = ctx.CENTER
            ctx.text_baseline = ctx.MIDDLE
            ctx.font_size = 50
            ctx.font = ctx.get_font_name(5)

            ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

          
          
            for i in range(17, 24 + 1):
                leds.set_rgb(i, 0, 1, 0)
           


            #circle StartGame
            ctx.arc(0, 170, 100, 0, tau, 0)
            ctx.rgb(0, 145/255, 12/255)
            ctx.fill()

            #circle DemoMode
            ctx.arc(170 * 0.63, -170 * 0.77, 100, 0, tau, 0)
            if self.DEMO_MODE:
                ctx.rgb(219/255, 0/255, 179/255)
                for i in range(1, 7  + 1):
                    leds.set_rgb(i, 219/255, 0/255, 179/255)
            else:
                ctx.rgb(205/255, 219/255, 0/255)
                for i in range(1, 7  + 1):
                    leds.set_rgb(i, 205/255, 219/255, 0/255)

            ctx.fill()

            #circle Controls
            ctx.arc(-170 * 0.54, -170 * 0.84, 100, 0, tau, 0)
            if self.CONTROL_TYPE == 0:
                ctx.rgb(230/255, 230/255, 230/255)
                for i in range(32, 38  + 1):
                    leds.set_rgb(i, 230/255, 230/255, 230/255)
                
            if self.CONTROL_TYPE == 1:
                ctx.rgb(53/255, 87/255, 240/255)
                for i in range(32, 38  + 1):
                    leds.set_rgb(i, 53/255, 87/255, 240/255)

            ctx.fill()




            leds.update()

            ctx.rgb(1,1,1)
            ctx.move_to(0, 0)
            ctx.save()
            ctx.text("Menu")
            ctx.restore()

            ctx.font_size = 30
            ctx.rgb(0,0,0)
            ctx.move_to(0, 94)
            ctx.save()
            ctx.text("Start Game")
            ctx.restore()
            
            
            ctx.font_size = 30
            ctx.rgb(0,0,0)
            ctx.move_to(0, 95)
            ctx.rotate(-2.45)
            ctx.save()
            if self.DEMO_MODE:
                ctx.text("Demo Mode")
            else:
                ctx.text("Standard")

            
            ctx.font_size = 30
            ctx.rgb(0,0,0)
            ctx.move_to(0, 95)
            ctx.rotate(5)#watch out, rotation from previous is still applied
            ctx.save()
            if self.CONTROL_TYPE == 0:
                ctx.text("IMU")
            if self.CONTROL_TYPE == 1:
                ctx.text("Touch")
                
                

            ctx.restore()


    def _draw_bat(self, ctx: Context, player: Player):
        player.compute_angles()  
        pos = player.bat * (3.14/2)
        
        #one Side
        ctx.rotate(pos + player.width)
        ctx.arc(0, 0, 118, 0, tau/2, 0)
        ctx.rgb(0, 0, 0)
        ctx.fill()
        
        ctx.rotate((pos + player.width ) * -1)

        
        #other Side
        ctx.rotate((pos - player.width))
        ctx.arc(0, 0, 118, 0, tau/2, 0)
        ctx.rgb(0, 0, 0)
        ctx.fill()

       
        ctx.rotate((pos - player.width) * -1)

        #center fill
        ctx.arc(0, 0, 110, 0, tau, 0)
        ctx.rgb(0, 0, 0)
        ctx.fill()


    def load_level(self, lvl):
        
        bricks = []
        with open(self.LEVEL_FILE, 'rb') as f:

            struct.unpack('H', f.read(2))#first two bytes, level count
           
            # Skip the data of preceding levels
            for _ in range(lvl - 1):
                num_bricks, = struct.unpack('H', f.read(2))
               
                f.seek(num_bricks * 12, 1)  # x, y, r, g, b, type: 4+4+1+1+1+1 = 12 bytes

            num_bricks, = struct.unpack('H', f.read(2))
           
            for _ in range(num_bricks):
                x, y, r, g, b, brick_type = struct.unpack('2i3BB', f.read(12))
                #2i = x , y
                #3B = RGB 
                #B = Brick Type
                color_hex = '{:02X}{:02X}{:02X}'.format(r, g, b)
                type_str = "normal" if brick_type == 1 else "unknown"
                x = x -10#current level generation is not quite in the middle
                brick = Brick(x, y,self.BRICK_WIDTH, self.BRICK_HEIGHT, color_hex, type_str)
                bricks.append(brick)
        
        return bricks

    
    def does_level_exist(self, level_number):
       
        with open(self.LEVEL_FILE, 'rb') as f:
            total_levels, = struct.unpack('H', f.read(2))
        return level_number <= total_levels

    
        # #Random Bricks
        # for y in range(-120, 0, self.BRICK_HEIGHT):
        #     for x in range(-100, 100, self.BRICK_WIDTH):
                
        #         brick_center_x = x + self.BRICK_WIDTH / 2
        #         brick_center_y = y + self.BRICK_HEIGHT / 2
                
        #         distance_to_center = math.sqrt(brick_center_x**2 + brick_center_y**2)
                
        #         if distance_to_center <= 100:
        #             brick = Brick(x, y, self.BRICK_WIDTH, self.BRICK_HEIGHT)
        #             bricks.append(brick)
        


    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self.cap_state = captouch.read()

        if sys_buttons.get_left() == sys_buttons.PRESSED_DOWN or sys_buttons.get_left() == sys_buttons.PRESSED_LEFT:
            self.set_leds((0,0,0))
            self.CURRENT_STATE = 2

        if self.DEMO_MODE and not self.next_level_pressed and sys_buttons.get_left() ==  sys_buttons.PRESSED_RIGHT:
            self.next_level_pressed = True
            for brick in self.bricks:
                brick.is_hit = True

        if  self.next_level_pressed and sys_buttons.get_left() == sys_buttons.NOT_PRESSED:#0 => sys_buttons.No button Pressed
            self.next_level_pressed = False


        if self.CURRENT_STATE == 0:

            self.player.update_input(ins, self.CONTROL_TYPE)

            if self.start_button_pressed and not self.cap_state.petals[5].pressed:
                self.start_button_pressed = False

            if not self.start_button_pressed and self.ball_sticking and  self.cap_state.petals[5].pressed:
                self.ball_sticking = False
                for i in range(17, 24 + 1):
                    leds.set_rgb(i, 0, 0, 0)
                leds.update()
            
            if not self.ball_sticking:

                x = self.p_x + self.v_x * delta_ms / 100.0
                y = self.p_y + self.v_y * delta_ms / 100.0

                self.check_collision(self.player)


                if x**2 + y**2 < (120 - 5) ** 2:
                    self.p_x = x
                    self.p_y = y
                else:
                    magnitude = (x**2 + y**2)**0.5
                    nx = x / magnitude
                    ny = y / magnitude

                    # Reflect the velocity vector over the normal
                    dot_product = self.v_x * nx + self.v_y * ny
                    self.v_x -= 2 * dot_product * nx
                    self.v_y -= 2 * dot_product * ny

                    # Move the point back inside the circle slightly to avoid multiple collisions
                    self.p_x = (120 - self.BALL_SIZE) * nx
                    self.p_y = (120 - self.BALL_SIZE) * ny
                    
                    if self.p_y > 0 and not self.DEMO_MODE:
                        #lost
                        self.SCORE_TEXT = "Game Over"
                        self.set_leds((1,0,0))
                        self.CURRENT_STATE  = 1
                
            else:
               middle_angle = (self.player.bat_start_angle + self.player.bat_end_angle) / 2
               self.p_x = 110 * math.cos(middle_angle)  # Cosine for x-coordinate
               self.p_y = 110 * math.sin(middle_angle)  # Sine for y-coordinate

            
                    

            self.reset_bricks_if_all_hit()

        if self.CURRENT_STATE == 1:
                if self.score_timer > 60 * 1:#60 ticks = 1 second??
                    self.score_timer = 0
                    self.CURRENT_STATE = 2
                    self.set_leds((0,0,0))
                    self.reset_bricks()

        if self.CURRENT_STATE == 2:
           
            if self.cap_state.petals[5].pressed:
                self.CURRENT_STATE = 0
                self.set_leds((0,0,0))
                self.reset_bricks()
                self.stick_ball()
                self.start_button_pressed = True
            if self.cap_state.petals[1].pressed and not self.demo_button_pressed:
                self.DEMO_MODE = not self.DEMO_MODE
                self.demo_button_pressed = True
            if not self.cap_state.petals[1].pressed:
                self.demo_button_pressed = False
            if self.cap_state.petals[9].pressed and not self.control_button_pressed:
                if self.CONTROL_TYPE == 0:
                    self.CONTROL_TYPE = 1
                else:
                    self.CONTROL_TYPE = 0
                self.control_button_pressed = True
            if not self.cap_state.petals[9].pressed:
                self.control_button_pressed = False
            
    


    def check_collision(self, player: Player):
        ball_angle = math.atan2(self.p_y, self.p_x)


        if player.bat_start_angle <= ball_angle <= player.bat_end_angle:
            
            if self.p_x**2 + self.p_y**2 < (120 - 20) ** 2:
                pass  # Ball is inside the safe region, so no reflection is needed.
            else:
                # Compute the angle of the bat's center (relative to the circle's origin)
                bat_center_angle = (player.bat_start_angle + player.bat_end_angle) / 2

                # Compute the deviation of the ball's angle from the bat's center angle
                angle_deviation = ball_angle - bat_center_angle

                # Adjust the reflection angle based on the deviation
                reflection_angle = 2 * bat_center_angle - ball_angle + 5 * angle_deviation

                # Store the original speed of the ball
                original_speed = (self.v_x**2 + self.v_y**2)**0.5

                # Convert the reflection angle to a direction vector
                self.v_x = math.cos(reflection_angle) * original_speed
                self.v_y = math.sin(reflection_angle) * original_speed

                # Compute the unit normal at the point of collision
                magnitude = (self.p_x**2 + self.p_y**2)**0.5
                nx = self.p_x / magnitude
                ny = self.p_y / magnitude

                # Reflect the velocity vector over the normal
                dot_product = self.v_x * nx + self.v_y * ny
                self.v_x -= 2 * dot_product * nx
                self.v_y -= 2 * dot_product * ny

                # Move the ball back inside the circle slightly to avoid multiple collisions
                self.p_x = (120 - self.BALL_SIZE) * nx
                self.p_y = (120 - self.BALL_SIZE) * ny

                
        previous_x = self.p_x - self.v_x 
        previous_y = self.p_y - self.v_y 

        

        for brick in self.bricks:
            if not brick.is_hit:
                if (self.p_x - self.BALL_SIZE/2 <= brick.x + brick.width and
                    self.p_x + self.BALL_SIZE/2 >= brick.x and
                    self.p_y - self.BALL_SIZE/2 <= brick.y + brick.height and
                    self.p_y + self.BALL_SIZE/2 >= brick.y):

                    if self.v_x > 0 and (previous_x + self.BALL_SIZE/2 <= brick.x):
                        self.v_x = -self.v_x
                    elif self.v_x < 0 and (previous_x - self.BALL_SIZE/2 >= brick.x + brick.width):
                        self.v_x = -self.v_x
                    elif self.v_y > 0 and (previous_y + self.BALL_SIZE/2 <= brick.y):
                        self.v_y = -self.v_y
                    elif self.v_y < 0 and (previous_y - self.BALL_SIZE/2 >= brick.y + brick.height):
                        self.v_y = -self.v_y

                    brick.is_hit = True
                    r, g, b = brick.color 
                    self.set_leds((r/2,g/2,b/2))
                    break

                    

    def reset_bricks_if_all_hit(self):
        if all(brick.is_hit for brick in self.bricks):
            self.SCORE_TEXT = "Winner!"
            
            self.CURRENT_LVL  = self.CURRENT_LVL +1

            if self.does_level_exist(self.CURRENT_LVL):
                    
                self.bricks = self.load_level(self.CURRENT_LVL)
                self.reset_ball()
                self.set_leds((0,0,0))
                if not self.DEMO_MODE:
                    self.stick_ball()
            else:
                self.CURRENT_LVL  = 1
                self.bricks = self.load_level(self.CURRENT_LVL)
                if not self.DEMO_MODE:
                    self.CURRENT_STATE = 1
                    


    def reset_bricks(self):
        for brick in self.bricks:
            brick.is_hit = False
            #brick.color = brick.random_color()
        self.reset_ball()
       

    def reset_ball(self):
        self.v_x = 2.0
        self.v_y = -7.0
        self.p_x = 0
        self.p_y = 80

    def stick_ball(self):
        self.ball_sticking = True
        for i in range(17, 24 + 1):\
            leds.set_rgb(i, 1, 1, 1)
        leds.update()

    def set_leds(self, color: tuple):
        r, g, b = color 
        for i in range(1, 39 + 1):\
            leds.set_rgb(i, r, g, b)
        leds.update()

    def on_enter(self, vm: Optional[ViewManager]) -> None:
        super().on_enter(vm)
        self.set_leds((0,0,0))


class Player:
    def __init__(self, petal_no):
        self.PETAL_NO = petal_no
        self.bat = 0
        self.bat_start_angle = 0
        self.bat_end_angle = 0
        self.width = 1.8#not actual widh - used as angle
        # self.width = 2#Kids version
        self.last_imu_values =[]


    def update_input(self, ins: InputState , controlType):
           

            if controlType == 0:
                filter_size = 12
                self.last_imu_values.append(ins.imu.acc[1])
            else:
                filter_size = 3
                #print(f"touch:{ins.captouch.petals[self.PETAL_NO].position[0]/40000}")
                self.last_imu_values.append((ins.captouch.petals[self.PETAL_NO].position[0]/40000)*1.5)

            if len(self.last_imu_values) > filter_size:
                self.last_imu_values.pop(0)


            avg_imu = sum(self.last_imu_values) / len(self.last_imu_values)

            self.bat = (avg_imu / filter_size) * 1.5

            if self.bat < -1:
                self.bat = -1
            if self.bat > 1:
                self.bat = 1
            self.bat *= -1
        

    def compute_angles(self):
        pos = self.bat * (3.14/2)
        self.bat_end_angle = pos + self.width
        self.bat_start_angle = 3.14159 - (pos - self.width) * -1


class Brick:
    def __init__(self, x, y, width, height, color_hex, brick_type="normal"):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.color = self.hex_to_rgb(color_hex)
        self.brick_type = brick_type
        self.is_hit = False

    def random_color(self):
        colorSpread = 160
        r, g, b = random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)
        while not (abs(r - g) > colorSpread or abs(g - b) > colorSpread or abs(r - b) > colorSpread):
            r, g, b = random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)
        return r / 255, g / 255, b / 255
    
    @staticmethod
    def hex_to_rgb(hex_color):
        if hex_color.startswith("#"):
            hex_color = hex_color[1:]
        r = int(hex_color[0:2], 16)/255
        g = int(hex_color[2:4], 16)/255
        b = int(hex_color[4:6], 16)/255
        return r, g, b

    def draw(self, ctx: Context):
        if not self.is_hit:
            ctx.rectangle(self.x, self.y, self.width, self.height)
            ctx.rgb(*self.color)
            ctx.fill()



if __name__ == '__main__':
    st3m.run.run_view(Arkanoid(ApplicationContext()))

